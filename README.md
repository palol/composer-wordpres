# WordPress handling via Composer README

## Basic resources
- PHP Composer ([https://getcomposer.org/](https://getcomposer.org/))
- Composer in WordPress ([https://composer.rarst.net/](https://composer.rarst.net/))

### PHP Composer cheat sheet
- Check composer.json for errors: `composer validate`

### PHP Composer specific
- Version constraints checker ([https://semver.mwl.be/](https://semver.mwl.be/?package=madewithlove%2Fhtaccess-cli&constraint=dev-main&stability=stable))


## Requirements
- John P Bloch WordPress core ([https://github.com/johnpbloch/wordpress-core](https://github.com/johnpbloch/wordpress-core))